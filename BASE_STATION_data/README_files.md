Files are categorized by their base station.
Each of base station folders contains csv file with information about aggregated lightning strikes. 
Csv filename is timestamp for data acquisition time given in format <dd-mm-yyyy-'h'H> where H-hour.