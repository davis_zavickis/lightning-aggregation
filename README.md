# Table of contents
[TOC]

## 1. Execution of code
1. Open shell script *lightning_aggregation.sh* and overwrite correct R executable file PATH on server
2. Make *lightning_aggregation.sh* as cronjob every hour 10 min past the start (10:10, 11:10, 12:10, ...)

## 2. Architecture
1. ** *lightning_aggregation.sh* ** is the core script to control sql pull request, invoke aggregation script (written in R) etc.
2. ** *export_strike_data.sql* ** is sql request which spools results in *expdat.csv*. In case when there are no lightning srikes recorded during particular hour, it automatically exports single dummy strike which is located in Baltic sea NW from Irbene with peak current 9999 kA.
3. ** *peripheralAuto.R* ** does all the aggregation within given range (currently 10km radius) of all (44) base stations and store data according to name of station within folder /BASE_STATION_data/[name]
4. Statistical data are stored in csv files. Filename represents date and hour of aggregation. For example: 20-8-2019-h7_stat.csv stands for aggregated data within timeframe from 7am-8am (24 hour format) in 20th August 2019.
5. Folder ** */LVA* ** stores contour of border of Latvia in .shp or other formats. **Currently not used** in calculations. Can be used for visualizations.
6. Folder ** */Meteo_stacijas* ** holds coordinates of base stations. 
7. ** *data.ini* ** file is **not currently used** but can be used for simpler multiparameter initialization if needed.

![image](0001.jpg)