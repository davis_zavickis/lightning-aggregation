# Make sure that you have sp, rgeos, RColorBrewer, raster, rgdal, ggplot2 and broom packages installed! 

list.of.packages <- c("sp","rgeos","RColorBrewer","raster","ggplot2","broom","colorRamps","plot3D","rgl","deldir","e1071", "pracma")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
print(paste("Installed several missing packages: ",new.packages))

library(sp)
library(rgeos)
library(broom)
library(rgl)
library(maps)
library(maptools)
library(dplyr)
library(e1071)
library(pracma)

readData <- function(ini_filename){
  df <- read.table(ini_filename, header = FALSE)
  # assign("dir", paste(df[df$V1=="dir","V2"]), envir = .GlobalEnv)
  assign("exportpathdir", paste(df[df$V1=="exportpathdir","V2"]), envir = .GlobalEnv)
  assign("border", paste(df[df$V1=="BORDER","V2"]), envir = .GlobalEnv)
  
}
getStationPolygons <- function(file, countries, stations){
  x<-file$LONGITUDE
  y<-file$LATITUDE
  strike_coords <- cbind(x,y) %>% as.data.frame() %>% SpatialPoints()
  
  Hour = file$LAIKS[0:1]$hour
  Day = file$LAIKS[0:1]$mday
  Month = file$LAIKS[0:1]$mon+1
  Year = file$LAIKS[0:1]$year+1900

  countries.LKS92 <- spTransform(countries, CRS("+init=epsg:3059"))
  stations.LKS92 <- spTransform(stations, CRS("+init=epsg:3059"))
  proj4string(file) <- CRS("+init=epsg:4326")
  file.LKS92 <- spTransform(file, CRS("+init=epsg:3059"))
  proj4string(strike_coords) <- CRS("+init=epsg:4326")
  strike_coords <- spTransform(strike_coords, CRS("+init=epsg:3059"))
  DateField <- paste(Day,"-",Month,"-",Year,"-h", Hour, sep="")
  
  for(k in 1:NROW(stations@coords)){
    SPls <- lapply(k:k,
                    function(x) Polygons(list(
                    Polygon(cbind(x=c(stations.LKS92@coords[x,1]+10^4*cos(seq(0,2*pi, by= 2*pi/100))), 
                                 y=c(stations.LKS92@coords[x,2]+10^4*sin(seq(0,2*pi, by= 2*pi/100)))
                    ))), ID=paste(stations@data$GH_ID[x]))) %>% SpatialPolygons()
    df <- data.frame(value = seq(1,1), row.names=stations@data$GH_ID[k])
    SPDF <- SpatialPolygonsDataFrame(SPls, df)
    proj4string(SPDF) <- CRS("+init=epsg:3059")
    res <- over(strike_coords, SPDF) %>% complete.cases()
  
    file.LKS92@coords <- (file.LKS92@coords %>% abs())
    re1 <- file.LKS92[res,]

    getStatistics(re1, toString(stations$GH_ID[k]), DateField, Year, Month, Day, Hour)

  }
}
getDateField <- function(Year, Month, Day, Hour, file){
  date<-as.Date(file$DATE)
  file$TIME <- substr(file$TIME, 1, nchar(toString(file$TIME))-4)
  time <- as.POSIXlt(file$TIME, "%H:%M:%S", tz="UTC")
  fr<-as.data.frame(time)
  hour <- time$hour
  month<-as.numeric(format(date, "%m"))
  year<-as.numeric(format(date, "%Y"))
  day<-as.numeric(format(date, "%d"))
  ndati<-intersect(intersect(intersect(which(month==Month), which(Year==year)),which(day==Day)), which(hour==Hour))
  return(file[ndati,])
}
getStatistics <- function(data, station, DateField, Year, Month, Day, Hour){
  export_table <- as.data.frame(matrix(ncol=11, nrow=0))
  if(NROW(data)!=0){
    cds <- as.data.frame(data@coords)
    data <- data@data
    pos_data <- data[data$`PEAK CURRENT`>0, ]
    neg_data <- data[data$`PEAK CURRENT`<0, ]
    
    ABS_COUNT <- length(data$`PEAK CURRENT`)
    # NEG_COUNT <- length(neg_data$`PEAK CURRENT`)
    # POS_COUNT <- ABS_COUNT-NEG_COUNT
    
    # REL_NEG <- NEG_COUNT/ABS_COUNT
    # REL_POS <- NEG_COUNT/ABS_COUNT
    
    
    C_COUNT <- length(data[data$`C/G flag` == "C", ]$`C/G flag`)
    G_COUNT <- length(data[data$`C/G flag` == "G", ]$`C/G flag`)
    
    # C_REL <- C_COUNT/ABS_COUNT
    # G_REL <- G_COUNT/ABS_COUNT
    
    # C_NEG_COUNT <- length(neg_data[neg_data$`C/G flag` == "C", ]$`PEAK CURRENT`)
    # C_POS_COUNT <- length(pos_data[pos_data$`C/G flag` == "C", ]$`PEAK CURRENT`)
    # G_NEG_COUNT <- length(neg_data[neg_data$`C/G flag` == "G", ]$`PEAK CURRENT`)
    # G_POS_COUNT <- length(pos_data[pos_data$`C/G flag` == "G", ]$`PEAK CURRENT`)
    
    # C_NEG_REL <- C_NEG_COUNT/ABS_COUNT
    # C_POS_REL <- C_POS_COUNT/ABS_COUNT
    # G_NEG_REL <- G_NEG_COUNT/ABS_COUNT
    # G_POS_REL <- G_POS_COUNT/ABS_COUNT
    
    
    MAX_ABS <- max(abs(data$`PEAK CURRENT`))
    # MAX_NEG <- suppressWarnings(min(neg_data$`PEAK CURRENT`)) #change to NA if Inf
    # MAX_POS <- suppressWarnings(max(pos_data$`PEAK CURRENT`)) #change to NA if Inf
    # if(MAX_NEG == "Inf" || MAX_NEG == "-Inf"){MAX_NEG=NA}
    # if(MAX_POS == "Inf" || MAX_POS == "-Inf"){MAX_POS=NA}
    
    # MEAN_NEG <- suppressWarnings(mean(neg_data$`PEAK CURRENT`))
    # MEAN_POS <- suppressWarnings(mean(pos_data$`PEAK CURRENT`))
    
    PC10_COUNT <- length(data[abs(data$`PEAK CURRENT`)>10, ]$`PEAK CURRENT`)
    
    xc <- cds$LATITUDE-mean(cds$LATITUDE)
    yc <- cds$LONGITUDE-mean(cds$LONGITUDE)
    
    stat <- c(Year, Month, Day, Hour, 0, 0, ABS_COUNT, C_COUNT, G_COUNT, MAX_ABS, PC10_COUNT)
    export_table <- rbind(export_table, stat)
    export_table[export_table=="NaN"]<-''
  }
  else{
    stat <- c(Year, Month, Day, Hour, 0, 0, 0, 0, 0, '', 0)
    export_table <- rbind(export_table, stat)
  }
  colnames(export_table) <- c("YEAR", "MONTH", "DAY", "HOUR", "MINUTE", "SECONDS", "ABS_COUNT", "C_COUNT", "G_COUNT", "MAX", "PC10_COUNT")
  
  wd<-paste("BASE_STATION_data/",station,sep="")
  dir.create(file.path(paste(wd, sep="")), showWarnings = FALSE)
  
  write.csv(export_table, file=paste(wd,"/",DateField,"_stat.csv", sep = ""), row.names = FALSE)
}

# readData("data.ini")
dir<-getwd()
exportpathdir<-"/BASE_STATION_data"
border<-"LVA_adm0"

countries <- rgdal::readOGR(dsn = "LVA", layer = border) # R>v3.5
stations <- rgdal::readOGR(dsn = "Meteo_stacijas", layer = "LV_jaunas_stac.shp") # Use for R 3.5 or higher

if(class(try(paste(dir, '/expdat.csv', sep='') %>% read.table(sep = ','))) == "try-error"){
} else {
  file <- paste(dir, '/expdat.csv', sep='') %>% read.table(sep = ',')
}
print(file)

colnames(file) <- c("LAIKS", "LATITUDE", "LONGITUDE", "PEAK CURRENT", "C/G flag")
coordinates(file) <- c("LONGITUDE", "LATITUDE")
colnames(countries@polygons[[1]]@Polygons[[1]]@coords) <- c('lon', 'lat')
colnames(stations@coords) <- c('lon', 'lat')
print(file$LAIKS)

file$LAIKS = as.POSIXlt(file$LAIKS, format = "%d-%b-%y %I.%M.%OS %p", tz="UTC")

# # fileC <- getDateField(Year, Month, Day, Hour, file)  # Correct timeslots are already batched within pl/sql and cron
# if(NROW(file$LAIKS) == 0){
#   # placed as a dummy strike in order to avoid empty dataframe errors. dummy strike located somwhere in Baltic sea NW from Irbene
#   coords<-data.frame(cbind(57.772082, 21.47958))
#   names(coords)<-cbind("LATITUDE", "LONGITUDE")
#   data<-data.frame(cbind("2019-01-01 00:00:25", 1000, "1"))
#   names(data)<-cbind("LAIKS", "PEAK CURRENT", "C/G flag")
#   fileX<-SpatialPointsDataFrame(coords, data, coords.nrs = numeric(0), proj4string = CRS(as.character(NA)), match.ID = TRUE)
#   file<-fileX
# }
# 
# if(file$`PEAK CURRENT` == 9999){
#   print(as.POSIXlt(Sys.time(), tz = "UTC"))
#   timeNOW = as.POSIXlt(Sys.time(), tz = "UTC")
#   timeNOW$hour = timeNOW$hour-1
#   timeNOW$min = timeNOW$min+30
#   print(timeNOW)
# }

getStationPolygons(file, countries, stations)

