set embedded on
set SERVEROUTPUT ON
set colsep ,
-- set headsep "	"
set heading off
set trimspool on
set pagesize 50000
set linesize 500
set numwidth 50
set echo off
set feedback off
set underline off
set pagesize 0

-- spool D:\dzavickis\ZIBENS DATI\Latvia_2006-2017\Export_data\expdat.csv
spool expdat.csv

	DECLARE
		v_count NUMBER;
		ltstamp TIMESTAMP WITH TIME ZONE:= SYSTIMESTAMP - INTERVAL '1' HOUR;
		CURSOR dat IS SELECT LAIKS, LAT, LON, PEAK_CURRENT, CLOUD FROM clidata.ml_zb
		WHERE
		EXTRACT(YEAR FROM LAIKS) = EXTRACT(YEAR FROM ltstamp) AND
		EXTRACT(MONTH FROM LAIKS) = EXTRACT(MONTH FROM ltstamp) AND
		EXTRACT(DAY FROM LAIKS) = EXTRACT(DAY FROM ltstamp) AND
		EXTRACT(HOUR FROM LAIKS) = EXTRACT(HOUR FROM ltstamp)
		ORDER BY LAIKS;
		
	begin
		-- ltstamp := ltstamp - INTERVAL '1' HOUR;
		SELECT count(LAIKS) INTO v_count FROM clidata.ml_zb
		WHERE 
		EXTRACT(YEAR FROM LAIKS) = EXTRACT(YEAR FROM ltstamp) AND
		EXTRACT(MONTH FROM LAIKS) = EXTRACT(MONTH FROM ltstamp) AND
		EXTRACT(DAY FROM LAIKS) = EXTRACT(DAY FROM ltstamp) AND
		EXTRACT(HOUR FROM LAIKS) = EXTRACT(HOUR FROM ltstamp);
		IF v_count = 0 THEN
			-- dbms_output.put_line('LAIKS,'||'LAT,'||'LON,'||'PEAK_CURRENT,'||'CLOUD');
			dbms_output.put_line(CAST(ltstamp AS CHAR)||',57.772082,'||'21.47958,'||'9999,'||'1');
		ELSE
			FOR i IN dat
			LOOP
				dbms_output.put_line(i.LAIKS ||','||i.LAT||','||i.LON||','||i.PEAK_CURRENT||','||i.CLOUD);
			END LOOP;
		END IF;
	end;
	/

spool off;